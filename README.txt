********************************************************************
D R U P A L    M O D U L E
********************************************************************
Name: Disclaimer Redirect 
Author: Steve Purkiss
Sponsor: Open for Organisations [www.open4.org]
Drupal: 7.x
********************************************************************

DESCRIPTION:

This module allows for disclaimer redirect to happen before an
anonymous user can enter certain sections of the site.

Anonymous users will be stopped at particular entry points, after verification
they will receive a cookie to enter those sections of the site.

This module started off with code from the Age Verify module:
http://drupal.org/sandbox/jshamley/1734946

with this patch to enable internal URL redirection:
http://drupal.org/files/age_verify-1734946.patch

along with a patch from the popup_message for the page selection code:
http://drupal.org/files/added_page_visibility-1534560.patch


********************************************************************
REQUIREMENTS:

None.


********************************************************************
INSTALLATION:

* Install as usual, see http://drupal.org/documentation/install/modules-themes
  for further information.


********************************************************************
CONFIGURATION:

* Configure the module in Configuration » System » Disclaimer Redirect

You can configure the following options:

Status:
- Disclaimer Redirect Status (enabled/disabled)

Redirect settings:
- Cookie Timeout Length
- Failure Destination URL

Error message settings:
- Display an error message to the user after redirection (enabled/disabled)
- Access denied error text
- Display a link so the user can reset their decision
- Access denied error reset link text

Success message settings:
- Display a message to the user (enabled/disabled)
- Access granted message text

Pages:
- Enable for specific pages

Disclaimer text:
- Disclaimer title
- Disclaimer body

Form labels:
- Form label
- Text for FALSE
- Text for TRUE
- Help text
- Submit button text
