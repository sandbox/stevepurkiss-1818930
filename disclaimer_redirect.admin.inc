<?php
/**
 * @file
 * Callback for settings page
 */
function disclaimer_redirect_settings($form, &$form_state) {
  $form['disclaimer_redirect_status'] = array(
    '#type' => 'fieldset',
    '#title' => t('Status'),
  );
  $form['disclaimer_redirect_status']['disclaimer_redirect_enable'] = array(
    '#type' => 'radios',
    '#title' => t('Disclaimer Redirect Status'),
    '#default_value' => variable_get('disclaimer_redirect_enable', DISCLAIMER_REDIRECT_ENABLE),
    '#options' => array(
      1 => t('Enabled'),
      0 => t('Disabled'),
    ),
  );
  $form['disclaimer_redirect_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Redirect Settings'),
    '#description' => t('Set your disclaimer redirect functionality preferences here.'),
  );
  $form['disclaimer_redirect_settings']['disclaimer_redirect_cookie_timeout'] = array(
    '#type' => 'textfield',
    '#title' => 'Cookie Timeout Length',
    '#default_value' => variable_get('disclaimer_redirect_cookie_timeout', DISCLAIMER_REDIRECT_COOKIE_TIMEOUT),
    '#size' => 40,
    '#maxlength' => 255,
    '#required' => FALSE,
    '#description' => t('Enter a time (in days) for the cookie to be valid. Default is set to 1 day.'),
  );
  $form['disclaimer_redirect_settings']['disclaimer_redirect_cookie_domain'] = array(
    '#type' => 'textfield',
    '#title' => 'Cookie Domain',
    '#default_value' => variable_get('disclaimer_redirect_cookie_domain', DISCLAIMER_REDIRECT_COOKIE_DOMAIN),
    '#size' => 40,
    '#maxlength' => 255,
    '#required' => FALSE,
    '#description' => t('The domain that the cookie is available to. Setting the domain to \'www.example.com\' will make the cookie available in the www subdomain and higher subdomains. Cookies available to a lower domain, such as \'example.com\' will be available to higher subdomains, such as \'www.example.com\'. Older browsers still implementing the deprecated RFC 2109 may require a leading \'.\' to match all subdomains.'),
  );
  $form['disclaimer_redirect_settings']['disclaimer_redirect_failure_destination'] = array(
    '#type' => 'textfield',
    '#title' => t('Failure Destination URL'),
    '#default_value' => variable_get('disclaimer_redirect_failure_destination', DISCLAIMER_REDIRECT_FAILURE_DESTINATION),
    '#size' => 40,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#description' => t('Enter a URL for the user to be directed to if they fail verification. URL can be a local URL (for example "node/15" or a node\'s URL alias).'),
  );
  $form['disclaimer_redirect_settings']['error'] = array(
    '#type' => 'fieldset',
    '#title' => t('Error message settings'),
    '#description' => t('Set your error message preferences here (works only for internal redirects).'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['disclaimer_redirect_settings']['error']['disclaimer_redirect_access_error_enable'] = array(
    '#type' => 'radios',
    '#title' => t('Display an error message to the user after redirection'),
    '#default_value' => variable_get('disclaimer_redirect_access_error_enable', DISCLAIMER_REDIRECT_ACCESS_ERROR_ENABLE),
    '#options' => array(
      1 => t('Enabled'),
      0 => t('Disabled'),
    ),
  );
  $form['disclaimer_redirect_settings']['error']['disclaimer_redirect_access_error'] = array(
    '#type' => 'textfield',
    '#title' => t('Access denied error text'),
    '#default_value' => variable_get('disclaimer_redirect_access_error', DISCLAIMER_REDIRECT_ACCESS_ERROR),
    '#size' => 40,
    '#maxlength' => 255,
    '#required' => FALSE,
    '#description' => t('Enter the error message text to be displayed to the user if redirecting internally.'),
  );
  $form['disclaimer_redirect_settings']['error']['disclaimer_redirect_access_error_reset'] = array(
    '#type' => 'radios',
    '#title' => t('Display a link so the user can reset their decision'),
    '#default_value' => variable_get('disclaimer_redirect_access_error_reset', DISCLAIMER_REDIRECT_ACCESS_ERROR_RESET),
    '#options' => array(
      1 => t('Enabled'),
      0 => t('Disabled'),
    ),
  );
  $form['disclaimer_redirect_settings']['error']['disclaimer_redirect_access_error_link_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Access denied error reset link text'),
    '#default_value' => variable_get('disclaimer_redirect_access_error_link_text', DISCLAIMER_REDIRECT_ACCESS_ERROR_LINK_TEXT),
    '#size' => 40,
    '#maxlength' => 255,
    '#required' => FALSE,
    '#description' => t('Enter the text to display on the reset link.'),
  );
  $form['disclaimer_redirect_settings']['success'] = array(
    '#type' => 'fieldset',
    '#title' => t('Success message settings'),
    '#description' => t('Set your success message preferences here (works only for internal redirects).'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['disclaimer_redirect_settings']['success']['disclaimer_redirect_access_success_enable'] = array(
    '#type' => 'radios',
    '#title' => t('Display a message to the user'),
    '#default_value' => variable_get('disclaimer_redirect_access_success_enable', DISCLAIMER_REDIRECT_ACCESS_SUCCESS_ENABLE),
    '#options' => array(
      1 => t('Enabled'),
      0 => t('Disabled'),
    ),
  );
  $form['disclaimer_redirect_settings']['success']['disclaimer_redirect_access_success'] = array(
    '#type' => 'textfield',
    '#title' => t('Access granted message text'),
    '#default_value' => variable_get('disclaimer_redirect_access_success', DISCLAIMER_REDIRECT_ACCESS_SUCCESS),
    '#size' => 40,
    '#maxlength' => 255,
    '#required' => FALSE,
    '#description' => t('Enter the message text to be displayed to the user if redirecting internally.'),
  );
  $form['disclaimer_redirect_settings']['visibility']['path'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pages'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'visibility',
    '#weight' => 0,
  );

  $options = array(
    DISCLAIMER_REDIRECT_VISIBILITY_NOTLISTED => t('All pages except those listed'),
    DISCLAIMER_REDIRECT_VISIBILITY_LISTED => t('Only the listed pages'),
  );

  $description = t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));

  $access = user_access('use PHP for settings');
  $visibility = variable_get('disclaimer_redirect_visibility', DISCLAIMER_REDIRECT_VISIBILITY_NOTLISTED);
  if (isset($visibility ) && $visibility  == DISCLAIMER_REDIRECT_VISIBILITY_PHP && !$access) {
    $form['disclaimer_redirect_settings']['visibility']['path']['disclaimer_redirect_visibility'] = array(
      '#type' => 'value',
      '#value' => DISCLAIMER_REDIRECT_VISIBILITY_PHP,
    );
    $form['disclaimer_redirect_settings']['visibility']['path']['disclaimer_redirect_visibility_pages'] = array(
      '#type' => 'value',
      '#value' => variable_get('disclaimer_redirect_visibility_pages', ''),
    );
  }
  else {
    if (module_exists('php') && $access) {
      $options += array(DISCLAIMER_REDIRECT_VISIBILITY_PHP => t('Pages on which this PHP code returns <code>TRUE</code> (experts only)'));
      $title = t('Pages or PHP code');
      $description .= ' ' . t('If the PHP option is chosen, enter PHP code between %php. Note that executing incorrect PHP code can break your Drupal site.', array('%php' => '<?php ?>'));
    }
    else {
      $title = t('Pages');
    }

    $form['disclaimer_redirect_settings']['visibility']['path']['disclaimer_redirect_visibility'] = array(
      '#type' => 'radios',
      '#title' => t('Enable for specific pages'),
      '#options' => $options,
      '#default_value' => $visibility,
    );

    $form['disclaimer_redirect_settings']['visibility']['path']['disclaimer_redirect_visibility_pages'] = array(
      '#type' => 'textarea',
      '#default_value' => variable_get('disclaimer_redirect_visibility_pages', ''),
      '#description' => $description,
      '#title' => '<span class="element-invisible">' . $title . '</span>',
    );
  }
  $form['disclaimer_redirect_text'] = array(
    '#type' => 'fieldset',
    '#title' => t('Disclaimer Text'),
    '#description' => t('Set your disclaimer title and text here.'),
  );
  $form['disclaimer_redirect_text']['disclaimer_redirect_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Disclaimer title'),
    '#required' => FALSE,
    '#default_value' => variable_get('disclaimer_redirect_title', DISCLAIMER_REDIRECT_TITLE),
    '#description' => t('Note: If you change the title you will need to clear caches before changes will take effect.'),
  );
  $disclaimer_redirect_body = variable_get('disclaimer_redirect_body');
  $form['disclaimer_redirect_text']['disclaimer_redirect_body'] = array(
    '#type' => 'text_format',
    '#base_type' => 'textarea',
    '#title' => t('Disclaimer body'),
    '#default_value' => $disclaimer_redirect_body['value'],
    '#format' => isset($disclaimer_redirect_body['format']) ? $disclaimer_redirect_body['format'] : NULL,
    );
  $form['disclaimer_redirect_form'] = array(
    '#type' => 'fieldset',
    '#title' => t('Form labels'),
    '#description' => t('Set your disclaimer form labels here.'),
  );
  $form['disclaimer_redirect_form']['disclaimer_redirect_form_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Form label'),
    '#description' => t('Set the text you want displayed on the form label here.'),
    '#required' => FALSE,
    '#default_value' => variable_get('disclaimer_redirect_form_label', DISCLAIMER_REDIRECT_FORM_LABEL),
  );
  $form['disclaimer_redirect_form']['disclaimer_redirect_form_false'] = array(
    '#type' => 'textfield',
    '#title' => t('Text for FALSE'),
    '#description' => t('Set the text you want displayed on the disagree radio button here.'),
    '#required' => TRUE,
    '#default_value' => variable_get('disclaimer_redirect_form_false', DISCLAIMER_REDIRECT_FORM_FALSE),
  );
  $form['disclaimer_redirect_form']['disclaimer_redirect_form_true'] = array(
    '#type' => 'textfield',
    '#title' => t('Text for TRUE'),
    '#description' => t('Set the text you want displayed on the agree radio button here.'),
    '#required' => TRUE,
    '#default_value' => variable_get('disclaimer_redirect_form_true', DISCLAIMER_REDIRECT_FORM_TRUE),
  );
  $form['disclaimer_redirect_form']['disclaimer_redirect_form_help'] = array(
    '#type' => 'textfield',
    '#title' => t('Help text'),
    '#description' => t('Set the text you want displayed beneath the form radio buttons here.'),
    '#required' => FALSE,
    '#default_value' => variable_get('disclaimer_redirect_form_help', DISCLAIMER_REDIRECT_FORM_HELP),
  );
  $form['disclaimer_redirect_form']['disclaimer_redirect_form_submit'] = array(
    '#type' => 'textfield',
    '#title' => t('Submit button'),
    '#description' => t('Set the submit button text here.'),
    '#required' => TRUE,
    '#default_value' => variable_get('disclaimer_redirect_form_submit', DISCLAIMER_REDIRECT_FORM_SUBMIT),
  );

  return system_settings_form($form);
}